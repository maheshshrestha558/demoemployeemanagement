﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace Client.Pages.Account
{
    public class JavaScriptInterop
    {
        private readonly IJSRuntime _jsRuntime;

        public JavaScriptInterop(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public  ValueTask LoadCustomScript()
        {
            return _jsRuntime.InvokeVoidAsync("loadCustomScript");
        }
    }
}
