﻿if (!window.blazorStarted) {
    Blazor.start();
    window.blazorStarted = true;
}
window.loadCustomScript = function () {
  
    console.log('loadCustomScript called');
    const signUpButton = document.getElementById("signUp");
    const signInButton = document.getElementById("signIn");
    const container = document.getElementById("container");

    if (signUpButton && signInButton && container) {
        console.log('Elements found');
        signUpButton.addEventListener("click", () => {
            container.classList.add("right-panel-active");
            console.log('Sign up clicked');
        });

        signInButton.addEventListener("click", () => {
            container.classList.remove("right-panel-active");
            console.log('Sign in clicked');
        });
    } else {
        console.error('One or more elements not found');
    }
    window.signUpButtonClicked = function () {
        const container = document.getElementById("container");
        if (container) {
            container.classList.add("right-panel-active");
        }
    };

    window.signInButtonClicked = function () {

        const container = document.getElementById("container");
        if (container) {
            container.classList.remove("right-panel-active");
        }
    };



    console.log('Custom script loaded');
};
