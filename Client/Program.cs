using Blazored.LocalStorage;
using ClientLibrary.Authentication;
using ClientLibrary.Helpers;
using ClientLibrary.Services.Contracts;
using ClientLibrary.Services.Implementation;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Syncfusion.Blazor;
using Syncfusion.Blazor.Popups;
using Client;
using Client.ApplicationStates;
using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using BaseLibrary.Entities.Help;


var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<Apps>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddTransient<CustomHttpHandler>();

builder.Services.AddHttpClient("SystemApiClient", client =>
{
    client.BaseAddress = new Uri("https://localhost:7158");
}).AddHttpMessageHandler<CustomHttpHandler>();


//builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://localhost:7158") });


builder.Services.AddAuthorizationCore();
builder.Services.AddBlazoredLocalStorage();
builder.Services.AddScoped<GetHttpClient>();
builder.Services.AddScoped<LocalStorageServices>();
builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
builder.Services.AddScoped<IUserAccountServices, UserAccountServices>();
builder.Services.AddScoped<AllState>();

//general department/department/branch
builder.Services.AddScoped<IGenericServiceInterface<GeneralDepartment>, GenericServiceImplementation<GeneralDepartment>>();
builder.Services.AddScoped<IGenericServiceInterface<Department>, GenericServiceImplementation<Department>>();
builder.Services.AddScoped<IGenericServiceInterface<Branch>, GenericServiceImplementation<Branch>>();


//country/town/city
builder.Services.AddScoped<IGenericServiceInterface<Country>, GenericServiceImplementation<Country>>();
builder.Services.AddScoped<IGenericServiceInterface<City>, GenericServiceImplementation<City>>();
builder.Services.AddScoped<IGenericServiceInterface<Town>, GenericServiceImplementation<Town>>();

//Doctor
builder.Services.AddScoped<IGenericServiceInterface<Doctor>, GenericServiceImplementation<Doctor>>();

//sancation and types
builder.Services.AddScoped<IGenericServiceInterface<Sanction>, GenericServiceImplementation<Sanction>>();
builder.Services.AddScoped<IGenericServiceInterface<SanctionType>, GenericServiceImplementation<SanctionType>>();

//vacation and types
builder.Services.AddScoped<IGenericServiceInterface<Vacation>, GenericServiceImplementation<Vacation>>();
builder.Services.AddScoped<IGenericServiceInterface<VacationType>, GenericServiceImplementation<VacationType>>();

//overtime and types
builder.Services.AddScoped<IGenericServiceInterface<OverTime>, GenericServiceImplementation<OverTime>>();
builder.Services.AddScoped<IGenericServiceInterface<OvertimeType>, GenericServiceImplementation<OvertimeType>>();

//Employee
builder.Services.AddScoped<IGenericServiceInterface<Employee>, GenericServiceImplementation<Employee>>();



builder.Services.AddSyncfusionBlazor();
//Register Syncfusion license

Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzQzMjIwNEAzMjM2MmUzMDJlMzBQVUFMTlFobG1McVhCNnAxaXB3RjhtNU5iSFNzek5nSGFTYlJMRExUVkV3PQ==");

builder.Services.AddScoped<SfDialogService>();

await builder.Build().RunAsync();
