﻿using BaseLibrary.Entities;

namespace Client.ApplicationStates
{
    public class AllState
    {

        //scope action
        public Action? Action { get; set; }

        //GeneralDepartment
        public bool ShowGeneralDepartment { get; set; }
        public void GeneralDepartmentClicked()
        {
            ResetAllDepartment();
            ShowGeneralDepartment = true;
            Action?.Invoke();

        }

        //Department
        public bool ShowDepartment { get; set; }
        public void DepartmentClicked()
        {
            ResetAllDepartment();
            ShowDepartment = true;
            Action?.Invoke();

        }

        //Branch
        public bool ShowBranch{ get; set; }
        public void BranchClicked()
        {
            ResetAllDepartment();
            ShowBranch = true;
            Action?.Invoke();

        }

        //Country
        public bool ShowCountry{ get; set; }
        public void CountryClicked()
        {
            ResetAllDepartment();
            ShowCountry = true;
            Action?.Invoke();

        }
        //City
        public bool ShowCity{ get; set; }
        public void CityClicked()
        {
            ResetAllDepartment();
            ShowCity= true;
            Action?.Invoke();

        }
        //Town 
        public bool ShowTown{ get; set; }
        public void TownClicked()
        {
            ResetAllDepartment();
            ShowTown= true;
            Action?.Invoke();

        }

        //showUSer
        public bool ShowUser{ get; set; }
        public void UserClicked()
        {
            ResetAllDepartment();
            ShowUser= true;
            Action?.Invoke();

        }

        //show doctor
        public bool ShowHealth { get; set; }
        public void HealthClicked()
        {
            ResetAllDepartment();
            ShowHealth = true;
            Action?.Invoke();

        }


        //show showOverTime Type
        public bool showOverTimeType { get; set; }
        public void OverTimeTypeClicked()
        {
            ResetAllDepartment();
            showOverTimeType = true;
            Action?.Invoke();

        }

        
        //show showOverTime 
        public bool showOverTime { get; set; }
        public void OverTimeClicked()
        {
            ResetAllDepartment();
            showOverTime = true;
            Action?.Invoke();

        }

        //show showsanction Type
        public bool showsanctionType { get; set; }
        public void SanctionTypeClicked()
        {
            ResetAllDepartment();
            showsanctionType = true;
            Action?.Invoke();

        }


        //show showsanction 
        public bool showsanction { get; set; }
        public void SanctionClicked()
        {
            ResetAllDepartment();
            showsanction = true;
            Action?.Invoke();

        }

        //show showsanction Type
        public bool showVacationType { get; set; }
        public void VacationTypeClicked()
        {
            ResetAllDepartment();
            showVacationType = true;
            Action?.Invoke();

        }


        //show showsanction 
        public bool showvacation { get; set; }
        public void VacationClicked()
        {
            ResetAllDepartment();
            showvacation = true;
            Action?.Invoke();

        }

        //Employee
        public bool ShowEmployee { get; set; } = true;
        public void EmployeeClicked()
        {
            ResetAllDepartment();
            ShowEmployee = true;
            Action?.Invoke();

        }

        private void ResetAllDepartment()
        {
            ShowEmployee = false;
            ShowUser = false;
            ShowCountry = false;
            ShowCity = false;
            ShowTown = false;
            ShowBranch = false;
            ShowGeneralDepartment =false;
            ShowDepartment = false;
            showsanction = false;
            showOverTime = false;
            showOverTimeType = false;
            showsanctionType = false;
            showVacationType = false;
            showvacation = false;
            ShowHealth = false;


        }
    }
}
