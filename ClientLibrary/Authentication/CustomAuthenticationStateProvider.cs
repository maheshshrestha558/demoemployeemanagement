﻿using BaseLibrary.Dto;
using ClientLibrary.Helpers;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary.Authentication
{
    public class CustomAuthenticationStateProvider(LocalStorageServices localStorageServices):AuthenticationStateProvider
    {
        private readonly ClaimsPrincipal anonymous = new(new ClaimsIdentity());
        
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var stringToken = await localStorageServices.GetToken();
            if (string.IsNullOrEmpty(stringToken)) return await Task.FromResult(new AuthenticationState(anonymous));


            var deserializeToken=Serializations.DeserializeJsonString<UserSession>(stringToken);
            if(deserializeToken == null) 
            {
            return await Task.FromResult(new AuthenticationState(anonymous));
            }

            var getUserclaims = DecryptToken(deserializeToken.Token!);
            if(getUserclaims == null)
            {
                return await Task.FromResult(new AuthenticationState(anonymous));

            }


            var claimsPrincipal=setClaimPrincipal(getUserclaims);
            return await Task.FromResult(new AuthenticationState(claimsPrincipal));

        }


        public async Task UpdateAuthenticationState(UserSession userSession)
        {
            var claimsprincipal=new ClaimsPrincipal();
            if(userSession.Token !=null || userSession.RefreshToken != null)
            {
                var serializeSession=Serializations.SerializeObj(userSession);
                await localStorageServices.SetToken(serializeSession);
                var getUserClaims = DecryptToken(userSession.Token!);
                claimsprincipal=setClaimPrincipal(getUserClaims);

            }
            else
            {
                await localStorageServices.RemoveToken();
            }
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(claimsprincipal)));
        }


        public static ClaimsPrincipal setClaimPrincipal(CustomUserClaims cliams)
        {

            if (cliams.Email is null) return new ClaimsPrincipal();

            return new ClaimsPrincipal(new ClaimsIdentity(
                new List<Claim>
                {
                    new(ClaimTypes.NameIdentifier,cliams.Id!),
                    new(ClaimTypes.Name,cliams.Name!),
                    new(ClaimTypes.Email,cliams.Email!),
                    new(ClaimTypes.Role,cliams.Role!),
                    
                }, "JwtAuth"));

        }






        private static CustomUserClaims DecryptToken(string jwttoken) 
        {
            if (string.IsNullOrEmpty(jwttoken)) return new CustomUserClaims();

            var handler = new JwtSecurityTokenHandler();
            var token=handler.ReadJwtToken(jwttoken);
            var userId = token.Claims.FirstOrDefault(_ => _.Type == ClaimTypes.NameIdentifier);
            var name=token.Claims.FirstOrDefault(_=>_.Type == ClaimTypes.Name);
            var email = token.Claims.FirstOrDefault(_ =>_.Type == ClaimTypes.Email);
            var role = token.Claims.FirstOrDefault(_ =>_.Type == ClaimTypes.Role);

            return new CustomUserClaims(userId!.Value, name!.Value, email!.Value, role!.Value);

        }
    }
}
