﻿using BaseLibrary.Dto;
using BaseLibrary.Entities.role;
using BaseLibrary.Responses;
using server;


namespace ClientLibrary.Services.Contracts
{
    public interface IUserAccountServices
    {
        Task<GeneralResponse> CreateAsync(Register user);
        Task<LoginResponse> SignInAsync(Login user);
        Task<LoginResponse> RefreshTokenAsync(RefreshToken token);

        Task<List<ManageUser>> GetUser();
        Task<GeneralResponse> UpdateUser(ManageUser user);
        Task<List<Systemroles>> GetRoles();
        Task<GeneralResponse> DeleteUser(int id);

    }
}
