﻿
using BaseLibrary.Responses;

namespace ClientLibrary.Services.Contracts
{
    public interface IGenericServiceInterface<T>
    {

        Task<List<T>> GetAll(string baseUrl);
        Task<T> GetbyId(int id, string baseurl);
        Task<GeneralResponse> Insert(T item, string baseurl);
        Task<GeneralResponse> Update(T item, string baseurl);
        Task<GeneralResponse> Delete(int id,string baseurl);
    }
}
