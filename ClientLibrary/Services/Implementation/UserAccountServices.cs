﻿using BaseLibrary.Dto;
using BaseLibrary.Entities.role;
using BaseLibrary.Responses;
using ClientLibrary.Helpers;
using ClientLibrary.Services.Contracts;
using server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary.Services.Implementation
{
    public class UserAccountServices(GetHttpClient getHttpClient) : IUserAccountServices
    {
        public const string authurl = "api/Authentication";
        public async Task<GeneralResponse> CreateAsync(Register user)
        {
            var httpclient=getHttpClient.GetPublicHttpClient();
            var result = await httpclient.PostAsJsonAsync($"{authurl}/register", user);
            if(!result.IsSuccessStatusCode)
            {
                return new GeneralResponse(false, "Error Occured");
            }
            return await result.Content.ReadFromJsonAsync<GeneralResponse>();

        }

        public async Task<LoginResponse> SignInAsync(Login user)
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.PostAsJsonAsync($"{authurl}/Login", user);
            if (!result.IsSuccessStatusCode)
            {
                return new LoginResponse(false, "Error Occured");
            }
            return await result.Content.ReadFromJsonAsync<LoginResponse>()!;
        }


        public async Task<LoginResponse> RefreshTokenAsync(RefreshToken token)
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.PostAsJsonAsync($"{authurl}/refresh-token", token);
            if (!result.IsSuccessStatusCode)
            {
                return new LoginResponse(false, "Error Occured");
            }
            return await result.Content.ReadFromJsonAsync<LoginResponse>();
        }
        public async Task<List<ManageUser>> GetUser()
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.GetFromJsonAsync<List<ManageUser>>($"{authurl}/users");
            return result;
        }

        public async Task<GeneralResponse> UpdateUser(ManageUser user) 
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.PutAsJsonAsync($"{authurl}/update-user", user);
            if (!result.IsSuccessStatusCode)
            {
                return new GeneralResponse(false, "Error Occured");
            }
            return await result.Content.ReadFromJsonAsync<GeneralResponse>();
        }

        public async Task<List<Systemroles>> GetRoles()
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.GetFromJsonAsync<List<Systemroles>>($"{authurl}/roles");
            return result;
        }

        public async Task<GeneralResponse> DeleteUser(int id)
        {
            var httpclient = getHttpClient.GetPublicHttpClient();
            var result = await httpclient.DeleteAsync($"{authurl}/delete-user/{id}");
            if (!result.IsSuccessStatusCode)
            {
                return new GeneralResponse(false, "Error Occured");
            }
            return await result.Content.ReadFromJsonAsync<GeneralResponse>();
        }
    }
}
