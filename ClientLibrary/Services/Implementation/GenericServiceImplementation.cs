﻿using BaseLibrary.Responses;
using ClientLibrary.Helpers;
using ClientLibrary.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary.Services.Implementation
{
    public class GenericServiceImplementation<T>(GetHttpClient getHttpClient) : IGenericServiceInterface<T>
    {
        //delete by if
        public async Task<GeneralResponse> Delete(int id, string baseurl)
        {
            var httpClient = await getHttpClient.GetPrivateHttpClient();
            var response = await httpClient.DeleteAsync($"{baseurl}/delete/{id}");
            var result = await response.Content.ReadFromJsonAsync<GeneralResponse>();
            return result!;
        }
        //get all data
        public async Task<List<T>> GetAll(string baseUrl)
        {
            var httpClient = await getHttpClient.GetPrivateHttpClient();
            var result=await httpClient.GetFromJsonAsync<List<T>>($"{baseUrl}/all");
            return result!;
        }
        //read single id
        public async Task<T> GetbyId(int id, string baseurl)
        {
            var httpClient = await getHttpClient.GetPrivateHttpClient();

            var result = await httpClient.GetFromJsonAsync<T>($"{baseurl}/single/{id}");
            return result!;

        }

        //create
        public async Task<GeneralResponse> Insert(T item, string baseurl)
        {
            var httpClient=await getHttpClient.GetPrivateHttpClient();
            var response = await httpClient.PostAsJsonAsync($"{baseurl}/add", item);
            var result= await response.Content.ReadFromJsonAsync<GeneralResponse>();
            return result!;
        }

        // update
        public async Task<GeneralResponse> Update(T item, string baseurl)
        {
            var httpClient = await getHttpClient.GetPrivateHttpClient();
            var response = await httpClient.PutAsJsonAsync($"{baseurl}/update", item);
            var result = await response.Content.ReadFromJsonAsync<GeneralResponse>();
            return result!;

        }
    }
}
