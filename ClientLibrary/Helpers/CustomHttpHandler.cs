﻿using BaseLibrary.Dto;
using ClientLibrary.Services.Contracts;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary.Helpers
{
    public class CustomHttpHandler(GetHttpClient getHttpClient, LocalStorageServices localStorageServices,IUserAccountServices accountServices) :DelegatingHandler
    {
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            bool loginurl = request.RequestUri!.AbsoluteUri.Contains("Login");
            bool registerurl = request.RequestUri!.AbsoluteUri.Contains("register");
            bool refreshtokenrurl = request.RequestUri!.AbsoluteUri.Contains("refresh-token");
            //yedi login garda token xa vane teslai cancel garne 
            if(loginurl || registerurl ||refreshtokenrurl) return await base.SendAsync(request, cancellationToken);

            var result= await base.SendAsync(request, cancellationToken);
            //yedi status code 401 aayo avne feri refresh token bata token leraune
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {

                //get token form local
                var stringtoken = await localStorageServices.GetToken();
                if (stringtoken == null) return result;
                //check if header contains token
                string token = string.Empty;
                try
                {
                    token = request.Headers.Authorization!.Parameter!;
                }
                catch
                {

                }
                var deserialToken= Serializations.DeserializeJsonString<UserSession>(stringtoken);
                if(deserialToken == null) return result;


                if (string.IsNullOrEmpty(token))
                {
                    //yedi token null xa vane feri refreshtoken bate request garera token leraune
                    request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", deserialToken.Token);
                    return await base.SendAsync(request, cancellationToken);
                    
                }

                //cal for refresh token
                var newJWttoken = await GetReshToken(deserialToken.RefreshToken!);
                if(string.IsNullOrEmpty(newJWttoken)) return result;

                //new token asaign garne ani old xa vane hataune kam garxa yesle
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", deserialToken.Token);
                return await base.SendAsync(request, cancellationToken);


            }
            return result;
        }

        private async Task<string> GetReshToken(string refreshToken)
        {
            var result=await accountServices.RefreshTokenAsync(new RefreshToken() { Token = refreshToken });
            string serializedToken= Serializations.SerializeObj(new UserSession() { Token= result.Token,RefreshToken= result.RefreshToken});
            await localStorageServices.SetToken(serializedToken);
            return result.Token;
        }
    }
}
