﻿using BaseLibrary.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary.Helpers
{
    public class GetHttpClient(IHttpClientFactory httpClientFactory,LocalStorageServices localStorageServices)
    {
        private const string headerKey = "Authorization";
        

        public async Task<HttpClient> GetPrivateHttpClient()
        {
            var client= httpClientFactory.CreateClient("SystemApiClient");
            var stringToken = await localStorageServices.GetToken();

            if (string.IsNullOrEmpty(stringToken)) return client;

            var deserializetoken = Serializations.DeserializeJsonString<UserSession>(stringToken);
            if(deserializetoken == null) return client;

            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer",deserializetoken.Token);
            return client;

        }

        public HttpClient GetPublicHttpClient()
        {
            var client = httpClientFactory.CreateClient("SystemApiClient");
            client.DefaultRequestHeaders.Remove(headerKey);
            return client;
        }
    }
}
