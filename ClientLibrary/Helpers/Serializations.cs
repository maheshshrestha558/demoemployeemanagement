﻿using System.Text.Json;


namespace ClientLibrary.Helpers
{
    public static  class Serializations
    {
        public static string SerializeObj<T>(T model) => JsonSerializer.Serialize(model);

        public static T DeserializeJsonString<T>(string jsonstring) => JsonSerializer.Deserialize<T>(jsonstring);

        public static IList<T> DeserializeJsonStringList<T>(string jsonstring) => JsonSerializer.Deserialize<IList<T>>(jsonstring);
    }
}
