﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Help;
using BaseLibrary.Entities.Leave;
using BaseLibrary.Entities.role;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.DataContext
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }


        //General Department//Department // branch
        public DbSet<GeneralDepartment> GeneralDepartments { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Branch>  Branches{ get; set; }


        //country //city // Town
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }

        public DbSet<Town> Towns { get; set; }
        

        //Authentication //Role //SystemRole 
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Systemroles> Systemroles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<RefreshTokenInfo> RefreshTokenInfos { get; set; }


        //Other // Vacation // Sanction // Doctor //OverTime

        public DbSet<Vacation> Vacations { get; set; }

        public DbSet<VacationType> VacationTypes { get; set; }
        public DbSet<OverTime> OverTimes { get; set; }
        public DbSet<OvertimeType> OvertimeTypes { get; set; }
        public DbSet<Sanction> Sanctions { get; set; }
        public DbSet<SanctionType> SanctionTypes { get; set; }
        public DbSet<Doctor> Doctors { get; set; }





    }

}
