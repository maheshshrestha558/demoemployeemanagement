﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class BranchRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Branch>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            var branch = await appDbContext.Branches.FindAsync(id);
            if (branch is null)
            {
                return NotFound();
            }
            appDbContext.Branches.Remove(branch);
            await Commit();
            return Success();
        }

        public async Task<List<Branch>> GetAll()
        {
            return await appDbContext.Branches.AsNoTracking()
                .Include(x => x.Department)
                .ToListAsync();

        }

        public async Task<Branch> GetById(int id)
        {
            return await appDbContext.Branches.FindAsync(id);

        }

        public async Task<GeneralResponse> Insert(Branch entity)
        {
            if (!await CheckName(entity.Name)) return new GeneralResponse(false, "Branch already added");
            appDbContext.Branches.Add(entity);
            await Commit();
            return Success();
        }

        public async Task<GeneralResponse> Update(Branch entity)
        {
            var branch = await appDbContext.Branches.FindAsync(entity.Id);
            if (branch == null)
            {
                return NotFound();
            }

            // Detach the existing entity to avoid tracking conflicts
            appDbContext.Entry(branch).State = EntityState.Detached;
            branch.Name = entity.Name;
            branch.DepartmentId = entity.DepartmentId;
            // Attach the updated entity and mark it as modified
            appDbContext.Attach(entity);
            appDbContext.Entry(entity).State = EntityState.Modified;
            
            await Commit();
            return Success();

            //var dep = await appDbContext.Branches.FindAsync(entity.Id);
            //if (dep is null) return NotFound();
            //appDbContext.Branches.Update(entity);
            //await Commit();
            //return Success();
        }
        private static GeneralResponse NotFound() => new(false, "Sorry Data not found");

        private static GeneralResponse Success() => new(true, "process Completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();


        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.Departments.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }

}
