﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Help;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;


namespace ServerLibrary.Repository.Implementation
{
    public class OvertimeRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<OverTime>
    {
        public async  Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.OverTimes.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.OverTimes.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<OverTime>> GetAll()
        {
            try
            {
                return await appDbContext.OverTimes.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<OverTime>(); // Return empty list on error
            }
        }

        public async Task<OverTime> GetById(int id)
        {
            try
            {
                return await appDbContext.OverTimes.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(OverTime entity)
        {
            try
            {
                appDbContext.OverTimes.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(OverTime entity)
        {
            try
            {
                var obj = await appDbContext.OverTimes.
                    FirstOrDefaultAsync(eid => eid.EmployeeeId == entity.EmployeeeId);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.StartDate = entity.StartDate;
                obj.EndDate = entity.EndDate;
                obj.OvertimeTypeId = entity.OvertimeTypeId;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }
        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();
    }
}
