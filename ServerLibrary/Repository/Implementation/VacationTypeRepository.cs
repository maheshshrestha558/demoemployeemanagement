﻿using BaseLibrary.Entities.Leave;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;


namespace ServerLibrary.Repository.Implementation
{
    public class VacationTypeRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<VacationType>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.VacationTypes.FindAsync(id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.VacationTypes.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<VacationType>> GetAll()
        {
            try
            {
                return await appDbContext.VacationTypes.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<VacationType>(); // Return empty list on error
            }
        }

        public async Task<VacationType> GetById(int id)
        {
            try
            {
                return await appDbContext.VacationTypes.FindAsync(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(VacationType entity)
        {
            try
            {
                if (!await CheckName(entity.Name!)) return new GeneralResponse(false, "Vacation Type already added");
                appDbContext.VacationTypes.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(VacationType entity)
        {
            try
            {
                var obj = await appDbContext.VacationTypes.
                    FindAsync(entity.Id);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.Name = entity.Name;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }

        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();

        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.VacationTypes.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }
}
