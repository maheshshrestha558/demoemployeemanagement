﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;


namespace ServerLibrary.Repository.Implementation
{
    public class VacationRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Vacation>
    {
        public async  Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.Vacations.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.Vacations.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<Vacation>> GetAll()
        {
            try
            {
                return await appDbContext.Vacations.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<Vacation>(); // Return empty list on error
            }
        }

        public async Task<Vacation> GetById(int id)
        {
            try
            {
                return await appDbContext.Vacations.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(Vacation entity)
        {
            try
            {
                appDbContext.Vacations.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(Vacation entity)
        {
            try
            {
                var obj = await appDbContext.Vacations.
                    FirstOrDefaultAsync(eid => eid.EmployeeeId == entity.EmployeeeId);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.StartDate = entity.StartDate;
                obj.NumberofDays = entity.NumberofDays;
                obj.VacationTypeId = entity.VacationTypeId;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }

        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();
    }
}
