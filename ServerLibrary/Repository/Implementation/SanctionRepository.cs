﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;


namespace ServerLibrary.Repository.Implementation
{
    public class SanctionRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Sanction>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.Sanctions.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.Sanctions.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<Sanction>> GetAll()
        {
            try
            {
                return await appDbContext.Sanctions.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<Sanction>(); // Return empty list on error
            }
        }

        public async Task<Sanction> GetById(int id)
        {
            try
            {
                return await appDbContext.Sanctions.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(Sanction entity)
        {
            try
            {
                appDbContext.Sanctions.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(Sanction entity)
        {
            try
            {
                var obj = await appDbContext.Sanctions.
                    FirstOrDefaultAsync(eid => eid.EmployeeeId == entity.EmployeeeId);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.Date = entity.Date;
                obj.Punishment = entity.Punishment;
                obj.PunishmentDate = entity.PunishmentDate;
                obj.SanctionType = entity.SanctionType;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }
        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();
    }
}
