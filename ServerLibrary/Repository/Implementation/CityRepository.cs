﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class CityRepository (AppDbContext appDbContext) : IGenericRepositoryInterFace<City>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            var branch = await appDbContext.Cities.FindAsync(id);
            if (branch is null)
            {
                return NotFound();
            }
            appDbContext.Cities.Remove(branch);
            await Commit();
            return Success();
        }

        public async Task<List<City>> GetAll()
        {
            return await appDbContext.Cities.
                AsNoTracking()
                .Include(gx => gx.Country).
                ToListAsync();

        }

        public async Task<City> GetById(int id)
        {
            return await appDbContext.Cities.FindAsync(id);

        }

        public async  Task<GeneralResponse> Insert(City entity)
        {
            if (!await CheckName(entity.Name)) return new GeneralResponse(false, "Cities already added");
            appDbContext.Cities.Add(entity);
            await Commit();
            return Success();
        }

        public async Task<GeneralResponse> Update(City entity)
        {
            var dep = await appDbContext.Cities.FindAsync(entity.Id);
            if (dep == null)
            {
                return NotFound();
            }

            // Detach the existing entity to avoid tracking conflicts
            appDbContext.Entry(dep).State = EntityState.Detached;
            dep.Name = entity.Name;
            dep.CountryId = entity.CountryId;

            // Attach the updated entity and mark it as modified
            appDbContext.Attach(entity);
            appDbContext.Entry(entity).State = EntityState.Modified;

            await Commit();
            return Success();

            //var dep = await appDbContext.Countries.FindAsync(entity.Id);
            //if (dep is null) return NotFound();
            //appDbContext.Cities.Update(entity);
            //await Commit();
            //return Success();
        }

        private static GeneralResponse NotFound() => new(false, "Sorry Data not found");

        private static GeneralResponse Success() => new(true, "process Completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();


        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.Cities.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }
}
