﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class DepartmentRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Department>
    {
        public async  Task<GeneralResponse> DeleteById(int id)
        {
            var depa = await appDbContext.Departments.FindAsync(id);
            if (depa is null)
            {
                return NotFound();
            }
            appDbContext.Departments.Remove(depa);
            await Commit();
            return Success();
        }

        public async Task<List<Department>> GetAll()
        {
            return await appDbContext.Departments.AsNoTracking()
                .Include(gx => gx.GeneralDepartment).
                ToListAsync();
                 



        }

        public async Task<Department> GetById(int id)
        {
            return await appDbContext.Departments.FindAsync(id);

        }

        public async Task<GeneralResponse> Insert(Department entity)
        {
            if (!await CheckName(entity.Name)) return new GeneralResponse(false, "Department already added");
            appDbContext.Departments.Add(entity);
            await Commit();
            return Success();
        }

        public async Task<GeneralResponse> Update(Department entity)
        {
            var dep = await appDbContext.Departments.FindAsync(entity.Id);
            if (dep == null)
            {
                return NotFound();
            }
            

            // Detach the existing entity to avoid tracking conflicts
            appDbContext.Entry(dep).State = EntityState.Detached;

            dep.Name = entity.Name;
            dep.GeneralDepartmentId = entity.GeneralDepartmentId;


            // Attach the updated entity and mark it as modified
            appDbContext.Attach(entity);
            appDbContext.Entry(entity).State = EntityState.Modified;

            await Commit();
            return Success();

            //var dep = await appDbContext.Departments.FindAsync(entity.Id);
            //if (dep is null) return NotFound();
            //appDbContext.Departments.Update(entity);
            //dep.Name = entity.Name;
            //dep.GeneralDepartmentId = entity.GeneralDepartmentId;
            //await Commit();
            //return Success();
        }


        private static GeneralResponse NotFound() => new(false, "Sorry Data not found");

        private static GeneralResponse Success() => new(true, "process Completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();


        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.Departments.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }
}
