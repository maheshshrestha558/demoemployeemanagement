﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class TownRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Town>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            var branch = await appDbContext.Towns.FindAsync(id);
            if (branch is null)
            {
                return NotFound();
            }
            appDbContext.Towns.Remove(branch);
            await Commit();
            return Success();
        }

        public async Task<List<Town>> GetAll()
        {
            return await appDbContext.Towns.ToListAsync();

        }

        public async Task<Town> GetById(int id)
        {
            return await appDbContext.Towns.FindAsync(id);

        }

        public async Task<GeneralResponse> Insert(Town entity)
        {
            if (!await CheckName(entity.Name)) return new GeneralResponse(false, "Towns already added");
            appDbContext.Towns.Add(entity);
            await Commit();
            return Success();
        }

        public async Task<GeneralResponse> Update(Town entity)
        {
            var dep = await appDbContext.Towns.FindAsync(entity.Id);
            if (dep == null)
            {
                return NotFound();
            }

            // Detach the existing entity to avoid tracking conflicts
            appDbContext.Entry(dep).State = EntityState.Detached;

            // Attach the updated entity and mark it as modified
            appDbContext.Attach(entity);
            appDbContext.Entry(entity).State = EntityState.Modified;

            await Commit();
            return Success();

            //var dep = await appDbContext.Towns.FindAsync(entity.Id);
            //if (dep is null) return NotFound();
            //appDbContext.Towns.Update(entity);
            //await Commit();
            //return Success();
        }

        private static GeneralResponse NotFound() => new(false, "Sorry Data not found");

        private static GeneralResponse Success() => new(true, "process Completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();


        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.Towns.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }
}
