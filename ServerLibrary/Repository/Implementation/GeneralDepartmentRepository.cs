﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class GeneralDepartmentRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<GeneralDepartment>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            var dep = await appDbContext.GeneralDepartments.FindAsync(id);
            if(dep is null)
            {
                return NotFound();
            }
            appDbContext.GeneralDepartments.Remove(dep);
            await Commit();
            return Success();
        }

        public async Task<List<GeneralDepartment>> GetAll()
        {
            return await appDbContext.GeneralDepartments.ToListAsync();
        }

        public async Task<GeneralDepartment> GetById(int id)
        {
            return await appDbContext.GeneralDepartments.FindAsync(id);
        }

        public async Task<GeneralResponse> Insert(GeneralDepartment entity)
        {
            var checkifnull = await CheckName(entity.Name!);
            if (!checkifnull) 
                return new GeneralResponse(false, "General Department already added");
            appDbContext.GeneralDepartments.Add(entity);
            await Commit();
            return Success();
        }
        public async Task<GeneralResponse> Update(GeneralDepartment entity)
        {
            var dep = await appDbContext.GeneralDepartments.FindAsync(entity.Id);
            if (dep == null)
            {
                return NotFound();
            }

            // Detach the existing entity to avoid tracking conflicts
            appDbContext.Entry(dep).State = EntityState.Detached;

            // Attach the updated entity and mark it as modified
            appDbContext.Attach(entity);
            appDbContext.Entry(entity).State = EntityState.Modified;

            await Commit();
            return Success();
        }
        //public async Task<GeneralResponse> Update(GeneralDepartment entity)
        //{
        //    var dep = await appDbContext.GeneralDepartments.FindAsync(entity.Id);
        //    if(dep is null) return NotFound();
        //    appDbContext.GeneralDepartments.Update(entity);
        //    await Commit();
        //    return Success();
        //}


        private static GeneralResponse NotFound() => new(false, "Sorry Data not found");

        private static GeneralResponse Success() => new(true, "process Completed");

        private async Task Commit()=> await appDbContext.SaveChangesAsync();


        private async Task<bool> CheckName(string Name)
        {
            var item=  await appDbContext.GeneralDepartments.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null? true :false ;
        }

    }
}
