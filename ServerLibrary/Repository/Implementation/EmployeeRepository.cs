﻿using BaseLibrary.Entities;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class EmployeeRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Employee>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            var item=await appDbContext.Employees.FindAsync(id);
            if(item is null ) return NotFound();
            appDbContext.Employees.Remove(item);
            await commit();
            return Success();
        }

        public async Task<List<Employee>> GetAll()
        {
            var employees = await appDbContext.Employees
                .AsNoTracking()
                .Include(t => t.Town)
                .ThenInclude(b => b.City)
                .ThenInclude(c => c.Country)
                .Include(b => b.Branch)
                .ThenInclude(d => d.Department)
                .ThenInclude(gd => gd.GeneralDepartment).ToListAsync();
            return employees;
        }

        public async  Task<Employee> GetById(int id)
        {
            return await appDbContext.Employees.FindAsync(id);
        }

        public async Task<GeneralResponse> Insert(Employee entity)
        {
            if (!await CheckName(entity.Name!)) return new GeneralResponse(false, "Employee Already Added");
            appDbContext.Employees .Add(entity);
            await commit();
            return Success();
        }

        public async Task<GeneralResponse> Update(Employee entity)
        {
            var finduser=await appDbContext.Employees.FirstOrDefaultAsync(e=>e.Id == entity.Id);
            if (finduser is null) return new GeneralResponse(false, "employee does not exits");

            finduser.Name=entity.Name;
            finduser.Others = entity.Others;
            finduser.Address= entity.Address;
            finduser.TelephoneNumber = entity.TelephoneNumber;
            finduser.BranchId= entity.BranchId;
            finduser.TownId = entity.TownId;
            finduser.CivilId = entity.CivilId;
            finduser.FileNumber= entity.FileNumber;
            finduser.JobName= entity.JobName;
            finduser.Photo= entity.Photo;

            //appDbContext.Employees.Update(entity);
            await appDbContext.SaveChangesAsync();
            await commit();
            return Success();


        }

        private async Task commit() => await appDbContext.SaveChangesAsync();

        private static GeneralResponse NotFound() => new(false, "Sorry Data not Found");
        private static GeneralResponse Success() => new(true, "process complete");

        private async Task<bool> CheckName(string name)
        {
            var item=await appDbContext.Employees.FirstOrDefaultAsync(x=>x.Name!.ToLower().Equals(name.ToLower()));
            return item is null ? true : false;

        }

    }
}
