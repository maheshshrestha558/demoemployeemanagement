﻿using BaseLibrary.Entities.Help;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Implementation
{
    public class DoctorRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<Doctor>
    {
        

        public async Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.Doctors.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.Doctors.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<Doctor>> GetAll()
        {
            try
            {
                return await appDbContext.Doctors.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<Doctor>(); // Return empty list on error
            }
        }

        public async Task<Doctor> GetById(int id)
        {
            try
            {
                return await appDbContext.Doctors.FirstOrDefaultAsync(eid => eid.EmployeeeId == id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(Doctor entity)
        {
            try
            {
                appDbContext.Doctors.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(Doctor entity)
        {
            try
            {
                var obj = await appDbContext.Doctors.FirstOrDefaultAsync(eid => eid.EmployeeeId == entity.EmployeeeId);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.MedicalRecommendation = entity.MedicalRecommendation;
                obj.MedicalDiagnose = entity.MedicalDiagnose;
                obj.Date = entity.Date;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }

        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();
    }
}
