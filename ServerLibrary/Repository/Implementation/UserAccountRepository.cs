﻿using BaseLibrary.Dto;
using BaseLibrary.Entities;
using BaseLibrary.Entities.role;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServerLibrary.DataContext;
using ServerLibrary.Helpers;
using ServerLibrary.Repository.Contracts;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ServerLibrary.Repository.Implementation
{
    public class UserAccountRepository(IOptions<JwtSection> config,AppDbContext appDbContext) : IUserAccount
    {
        public async  Task<GeneralResponse> CreateAsync(Register user)
        {
            if (user is null) return new GeneralResponse(false, "model is empty");
            var checkuser = await FindUserbyEmail(user.Email!);
            if (checkuser != null) return new GeneralResponse(false, "user Registered already");

            //save user
            var applicationuser = await AddToDatabase(new ApplicationUser()
            {
                FullName=user.FullName,
                Email = user.Email,
                Password = BCrypt.Net.BCrypt.HashPassword(user.Password)
            });

            //check,crate and assign role
            var checkadminrole =  await appDbContext.Systemroles.FirstOrDefaultAsync(_ => _.Name!.Equals(constants.Admin));
            if(checkadminrole is null)
            {
                var createadminrole = await AddToDatabase(new Systemroles() { Name = constants.Admin });
                await AddToDatabase(new UserRole() { RoleId = createadminrole.Id, UserId = applicationuser.Id });
                return new GeneralResponse(true, "Account Created");
            }



            //check userrole
            var checkuserrole = await appDbContext.Systemroles.FirstOrDefaultAsync(_ => _.Name!.Equals(constants.User));
            Systemroles response = new();
            if (checkuserrole is null)
            {
                response = await AddToDatabase(new Systemroles() { Name = constants.User });
                await AddToDatabase(new UserRole() { RoleId = response.Id, UserId = applicationuser.Id });
            }
            else
            {

                await AddToDatabase(new UserRole() { RoleId = checkuserrole.Id, UserId = applicationuser.Id });

            }
            return new GeneralResponse(true, "Account Created");

        }

        public async Task<LoginResponse> SignInAsync(Login user)
        {
            if (user is null) return new LoginResponse(false, "Model is Empty");

            //verify password
            var applicationuser = await FindUserbyEmail(user.Email!);
            if (applicationuser is null) return new LoginResponse(false, "User Not Found!");


            if (!BCrypt.Net.BCrypt.Verify(user.Password, applicationuser.Password))
                return new LoginResponse(false, "Email/password not valid");

            var getuserRole = await FindUserRole(applicationuser.Id);
                //await appDbContext.UserRoles.FirstOrDefaultAsync(_=>_.UserId == applicationuser.Id);
            if (getuserRole is null) return new LoginResponse(false, "user role not found");

            var getroleName = await FindRoleName(getuserRole.RoleId);
            //appDbContext.Systemroles.FirstOrDefaultAsync(_ => _.Id == getuserRole.Id)
            if (getuserRole is null) return new LoginResponse(false, "user role not found");


            string jwttoken = GenerateToken(applicationuser, getroleName!.Name);
            string refreshToken = GenerateRefreshToken();

            //save the refresh token
            var finduser = await appDbContext.RefreshTokenInfos.FirstOrDefaultAsync(_ => _.UserId == applicationuser.Id);
            if(finduser is not null)
            {
                finduser!.Token = refreshToken;
                await appDbContext.SaveChangesAsync();
            }
            else
            {
                await AddToDatabase(
                    new RefreshTokenInfo()
                { Token = refreshToken,UserId=applicationuser.Id 
                }
                );

    
            }
            return new LoginResponse(true, "Login Successfully", jwttoken, refreshToken);
        }

        //generating token this way
        private string GenerateToken(ApplicationUser user,string role)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.Value.Key!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var userClaims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.FullName!),
                new Claim(ClaimTypes.Email,user.Email!),
                new Claim(ClaimTypes.Role,role)
            };

            var token = new JwtSecurityToken(
                issuer: config.Value.Issuer,
                audience: config.Value.Audience,
                claims: userClaims,
                //expires: DateTime.Now.AddDays(1),
                expires: DateTime.Now.AddSeconds(10),

                signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        //for refreshing the token

        private async Task<UserRole> FindUserRole(int userId) => await appDbContext.UserRoles.FirstOrDefaultAsync(_ => _.UserId == userId);

        private async Task<Systemroles> FindRoleName(int roleId) => await appDbContext.Systemroles.FirstOrDefaultAsync(_ => _.Id == roleId);
        private static string GenerateRefreshToken() => Convert.ToBase64String(RandomNumberGenerator.GetBytes(64));

        private async Task<ApplicationUser> FindUserbyEmail(string email)
        {
            return await appDbContext.ApplicationUsers.FirstOrDefaultAsync(_ => _.Email!.ToLower()!.Equals(email!.ToLower()));
        }

        private async Task<T> AddToDatabase<T>(T model)
        {
            var result = appDbContext.Add(model!);
            await appDbContext.SaveChangesAsync();
            return (T)result.Entity;
        }

        public async Task<LoginResponse> RefreshTokenAsync(RefreshToken token)
        {
            if (token is null) return new LoginResponse(false, "Model is empty");

            var findToken = await appDbContext.RefreshTokenInfos.FirstOrDefaultAsync(_ => _.Token!.Equals(token.Token));
            if (findToken is null) return new LoginResponse(false, "Refresh Token is Requried");

            //get user details
            var user = await appDbContext.ApplicationUsers.FirstOrDefaultAsync(_ => _.Id == findToken.UserId);
            if (user is null) return new LoginResponse(false, "Refresh Token couldnot be generated because user not found");


            var userRole = await FindUserRole(user.Id);
            var roleName = await FindRoleName(userRole.RoleId);
            string jwtToken = GenerateToken(user, roleName.Name!);
            string refreshtoken = GenerateRefreshToken();

            //check refreshtoken has that user or not
            var updateRefreshToken = await appDbContext.RefreshTokenInfos.FirstOrDefaultAsync(_ => _.UserId == user.Id);
            if (updateRefreshToken is null) return new LoginResponse(false, "Refresh Token couldnot be generated because user has not sign In");


            //if there
            updateRefreshToken.Token = refreshtoken;
            await appDbContext.SaveChangesAsync();
            return new LoginResponse(true, "Token refreshed successfully", jwtToken, refreshtoken);

        }

        public async Task<List<ManageUser>> GetUser()
        {
            var alluser = await GetApplicationUsers();
            var alluserroles = await userRoles();
            var allroles = await systemroles();

            if(alluserroles.Count == 0 || allroles.Count==0) return null;

            var users = new List<ManageUser>();
            foreach(var user in alluser)
            {
                var userroles=alluserroles.FirstOrDefault(u=>u.UserId==user.Id);
                var rolename=allroles.FirstOrDefault(u=>u.Id==userroles!.RoleId);
                users.Add(new ManageUser() { 
                UserId=user.Id,
                Name=user.FullName,
                Email=user.Email,
                Role=rolename!.Name
                });

            }
            return users;
        }



        public async Task<GeneralResponse> UpdateUser(ManageUser user)
        {
            var getRole=(await systemroles()).FirstOrDefault(r=>r.Name!.Equals(user.Role));
            var userRole = await appDbContext.UserRoles.FirstOrDefaultAsync(u => u.UserId == user.UserId);
            userRole!.RoleId = getRole!.Id;
            await appDbContext.SaveChangesAsync();
            return new GeneralResponse(true, "User Role Update Successfully");

        }

        public async Task<List<Systemroles>> GetRoles()
        {
           return await systemroles();
        }

        public async Task<GeneralResponse> DeleteUser(int id)
        {
            var user=await appDbContext.ApplicationUsers.FirstOrDefaultAsync(u=>u.Id==id);
            appDbContext.ApplicationUsers.Remove(user!);
            await appDbContext.SaveChangesAsync();
            return new GeneralResponse(true, "User Delete Successfully");


        }


        private async Task<List<Systemroles>> systemroles()=> await appDbContext.Systemroles.AsNoTracking().ToListAsync();
        private async Task<List<UserRole>> userRoles() => await appDbContext.UserRoles.AsNoTracking().ToListAsync();
        private async Task<List<ApplicationUser>> GetApplicationUsers() => await appDbContext.ApplicationUsers.AsNoTracking().ToListAsync();

    }
}
