﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using BaseLibrary.Responses;
using Microsoft.EntityFrameworkCore;
using ServerLibrary.DataContext;
using ServerLibrary.Repository.Contracts;


namespace ServerLibrary.Repository.Implementation
{
    public class SanctionTypeRepository(AppDbContext appDbContext) : IGenericRepositoryInterFace<SanctionType>
    {
        public async Task<GeneralResponse> DeleteById(int id)
        {
            try
            {
                var item = await appDbContext.SanctionTypes.FindAsync(id);
                if (item is null)
                {
                    return NotFound();
                }
                appDbContext.SanctionTypes.Remove(item);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Deletion failed");
            }
        }

        public async Task<List<SanctionType>> GetAll()
        {
            try
            {
                return await appDbContext.SanctionTypes.AsNoTracking().ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new List<SanctionType>(); // Return empty list on error
            }
        }

        public async Task<SanctionType> GetById(int id)
        {
            try
            {
                return await appDbContext.SanctionTypes.FindAsync(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null; // Return null on error
            }
        }

        public async Task<GeneralResponse> Insert(SanctionType entity)
        {
            try
            {
                if (!await CheckName(entity.Name!)) return new GeneralResponse(false, "Sanction Type already added");
                appDbContext.SanctionTypes.Add(entity);
                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Insert failed");
            }
        }

        public async Task<GeneralResponse> Update(SanctionType entity)
        {
            try
            {
                var obj = await appDbContext.SanctionTypes.
                    FindAsync(entity.Id);
                if (obj == null)
                {
                    return NotFound();
                }

                // Detach the existing entity to avoid tracking conflicts
                appDbContext.Entry(obj).State = EntityState.Detached;

                // Update properties
                obj.Name = entity.Name;

                // Attach and mark the updated entity as modified
                appDbContext.Attach(entity);
                appDbContext.Entry(entity).State = EntityState.Modified;

                await Commit();
                return Success();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return new GeneralResponse(false, "Update failed");
            }
        }

        private static GeneralResponse NotFound() => new(false, "Sorry, Data not found");

        private static GeneralResponse Success() => new(true, "Process completed");

        private async Task Commit() => await appDbContext.SaveChangesAsync();

        private async Task<bool> CheckName(string Name)
        {
            var item = await appDbContext.SanctionTypes.FirstOrDefaultAsync(x => x.Name!.ToLower().Equals(Name.ToLower()));
            return item is null;
        }
    }
}
