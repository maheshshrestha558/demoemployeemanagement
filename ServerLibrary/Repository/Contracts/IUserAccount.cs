﻿using BaseLibrary.Dto;
using BaseLibrary.Entities.role;
using BaseLibrary.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLibrary.Repository.Contracts
{
    public interface IUserAccount
    {
        Task<GeneralResponse> CreateAsync(Register user);
        Task<LoginResponse> SignInAsync(Login user);
        Task<LoginResponse> RefreshTokenAsync(RefreshToken token);
        Task<List<ManageUser>> GetUser();
        Task<GeneralResponse> UpdateUser(ManageUser user);
        Task<List<Systemroles>> GetRoles();
        Task<GeneralResponse> DeleteUser(int id);

    }
}
