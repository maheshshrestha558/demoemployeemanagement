using BaseLibrary.Entities;
using BaseLibrary.Entities.Help;
using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ServerLibrary.DataContext;
using ServerLibrary.Helpers;
using ServerLibrary.Repository.Contracts;
using ServerLibrary.Repository.Implementation;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//jwtmapper
builder.Services.Configure<JwtSection>(builder.Configuration.GetSection("JwtSection"));
var jwtSection = builder.Configuration.GetSection(nameof(JwtSection)).Get<JwtSection>();




//starting
builder.Services.AddDbContext<AppDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")
        ?? throw new InvalidOperationException("sorry, your db connection is not found"));
});



//jwtmapper to appsetting

builder.Services.AddAuthentication(
    option =>
    {
        option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

    }).AddJwtBearer(
    options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateIssuerSigningKey = true,
            ValidateLifetime = true,
            ValidIssuer = jwtSection!.Issuer,
            ValidAudience = jwtSection.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection.Key!)),



        };
    }
    );








//addscoped
//builder.Services.AddTransient<IUserAccount, UserAccountRepository>();
builder.Services.AddScoped<IUserAccount, UserAccountRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<GeneralDepartment>, GeneralDepartmentRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<Department>, DepartmentRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<Branch>, BranchRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<Country>, CounrtyRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<City>, CityRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<Town>, TownRepository>();

builder.Services.AddScoped<IGenericRepositoryInterFace<Doctor>, DoctorRepository>();

builder.Services.AddScoped<IGenericRepositoryInterFace<OverTime>, OvertimeRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<OvertimeType>, OvertimeTypeRepository>();

builder.Services.AddScoped<IGenericRepositoryInterFace<Sanction>, SanctionRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<SanctionType>, SanctionTypeRepository>();

builder.Services.AddScoped<IGenericRepositoryInterFace<Vacation>, VacationRepository>();
builder.Services.AddScoped<IGenericRepositoryInterFace<VacationType>, VacationTypeRepository>();

builder.Services.AddScoped<IGenericRepositoryInterFace<Employee>, EmployeeRepository>();




builder.Services.AddCors(
    
    options =>
    {
        options.AddPolicy("AllowBlazorWasm",
            builder => builder
            .WithOrigins("http://localhost:5015", "https://localhost:7050")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
    });








var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("AllowBlazorWasm");
app.UseStaticFiles();



app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
