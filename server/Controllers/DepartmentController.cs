﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Department")]
    [ApiController]
    public class DepartmentController(IGenericRepositoryInterFace<Department> genericRepositoryInterFace)
        : GenericController<Department>(genericRepositoryInterFace)
    {
    }
}
