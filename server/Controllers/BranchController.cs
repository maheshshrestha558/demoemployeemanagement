﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Branch")]
    [ApiController]
    public class BranchController(IGenericRepositoryInterFace<Branch> genericRepositoryInterFace)
        : GenericController<Branch>(genericRepositoryInterFace)
    {
    }
}
