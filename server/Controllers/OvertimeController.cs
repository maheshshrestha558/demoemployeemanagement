﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Overtime")]
    [ApiController]
    public class OvertimeController(IGenericRepositoryInterFace<OverTime> genericRepositoryInterFace)
        : GenericController<OverTime>(genericRepositoryInterFace)
    {
    }
}
