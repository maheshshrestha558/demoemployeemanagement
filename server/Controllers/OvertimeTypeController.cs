﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/OvertimeType")]
    [ApiController]
    public class OvertimeTypeController(IGenericRepositoryInterFace<OvertimeType> genericRepositoryInterFace)
        : GenericController<OvertimeType>(genericRepositoryInterFace)
    {
    }
}
