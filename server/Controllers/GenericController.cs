﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T>(IGenericRepositoryInterFace<T> genericRepositoryInterFace) 
        : Controller where T:class
    {
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await genericRepositoryInterFace.GetAll());
        }


        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0) return BadRequest("Invalid Request sent");
            return Ok(await genericRepositoryInterFace.DeleteById(id));
        }


        [HttpGet("single/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0) return BadRequest("Invalid Request sent");
            return Ok(await genericRepositoryInterFace.GetById(id));
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add(T model)
        {
            if (model is null ) return BadRequest("Invalid Request sent");
            return Ok(await genericRepositoryInterFace.Insert(model));
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update(T model)
        {
            if (model is null) return BadRequest("Invalid Request sent");
            return Ok(await genericRepositoryInterFace.Update(model));
        }


    }
}
