﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/GeneralDepartment")]
    [ApiController]
    public class GeneralDepartmentController(IGenericRepositoryInterFace<GeneralDepartment> genericRepositoryInterFace)
        : GenericController<GeneralDepartment>(genericRepositoryInterFace)
    {
    }
}
