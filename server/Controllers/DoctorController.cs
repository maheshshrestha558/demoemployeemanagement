﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Help;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Doctor")]
    [ApiController]
    public class DoctorController(IGenericRepositoryInterFace<Doctor> genericRepositoryInterFace)
        : GenericController<Doctor>(genericRepositoryInterFace)
    {
    }
}
