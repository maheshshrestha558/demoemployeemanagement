﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Country")]
    [ApiController]
    public class CountyController(IGenericRepositoryInterFace<Country> genericRepositoryInterFace)
        : GenericController<Country>(genericRepositoryInterFace)
    {
    }
}
