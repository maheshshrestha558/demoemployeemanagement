﻿using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/SanctionType")]
    [ApiController]
    public class SanctionTypeController(IGenericRepositoryInterFace<SanctionType> genericRepositoryInterFace)
        : GenericController<SanctionType>(genericRepositoryInterFace)
    {
    }
}
