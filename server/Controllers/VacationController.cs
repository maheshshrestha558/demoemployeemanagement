﻿using BaseLibrary.Entities;
using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Vacation")]
    [ApiController]
    public class VacationController(IGenericRepositoryInterFace<Vacation> genericRepositoryInterFace)
        : GenericController<Vacation>(genericRepositoryInterFace)
    {
    }
}
