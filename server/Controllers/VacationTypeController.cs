﻿using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/VacationType")]
    [ApiController]
    public class VacationTypeController(IGenericRepositoryInterFace<VacationType> genericRepositoryInterFace)
        : GenericController<VacationType>(genericRepositoryInterFace)
    {
    }
}
