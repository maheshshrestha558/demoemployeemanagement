﻿using BaseLibrary.Entities.Help;
using BaseLibrary.Entities.Leave;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Sanction")]
    [ApiController]
    public class SanctionController(IGenericRepositoryInterFace<Sanction> genericRepositoryInterFace)
        : GenericController<Sanction>(genericRepositoryInterFace)
    {
    }
}
