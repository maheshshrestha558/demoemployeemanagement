﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Town")]
    [ApiController]
    public class TownsController(IGenericRepositoryInterFace<Town> genericRepositoryInterFace)
        : GenericController<Town>(genericRepositoryInterFace)
    {
    }
}
