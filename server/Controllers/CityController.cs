﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/City")]
    [ApiController]
    public class CityController(IGenericRepositoryInterFace<City> genericRepositoryInterFace)
        : GenericController<City>(genericRepositoryInterFace)
    {
    }
}
