﻿using BaseLibrary.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Authentication")]
    [ApiController]
    [AllowAnonymous]
    public class AuthenticationController(IUserAccount accountInterface) : ControllerBase
    {
        

        [HttpPost("register")]
        public async Task<IActionResult> CreateAccount(Register user)
        {
            if (user == null) return BadRequest("Model is Empty");

            var result =await accountInterface.CreateAsync(user);
            return Ok(result);
        }



        [HttpPost("Login")]
        public async Task<IActionResult> Login(Login user)
        {
            if (user == null) return BadRequest("Model is Empty");

            var result = await accountInterface.SignInAsync(user);
            return Ok(result);
        }



        [HttpPost("refresh-token")]
        public async Task<IActionResult> refreshtoken(RefreshToken user)
        {
            if (user == null) return BadRequest("Model is Empty");

            var result = await accountInterface.RefreshTokenAsync(user);
            return Ok(result);
        }



        [HttpGet("users")]
        public async Task<IActionResult> GetUserAsync()
        {
            var users = await accountInterface.GetUser();
            if (users == null) return NotFound();
            return Ok(users);
        }

        [HttpPut("update-user")]
        public async Task<IActionResult> UpdateUser(ManageUser manageUser)
        {
            var result = await accountInterface.UpdateUser(manageUser);
            return Ok(result);
        }

        [HttpGet("roles")]
        public async Task<IActionResult> GetRoles()
        {
            var users = await accountInterface.GetRoles();
            if (users == null) return NotFound();
            return Ok(users);
        }

        [HttpGet("delete-user")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var users = await accountInterface.DeleteUser(id);
            return Ok(users);
        }





    }
}
