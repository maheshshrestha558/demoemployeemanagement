﻿using BaseLibrary.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerLibrary.Repository.Contracts;

namespace server.Controllers
{
    [Route("api/Employee")]
    [ApiController]
    public class EmployeeController(IGenericRepositoryInterFace<Employee> genericRepositoryInterFace)
        : GenericController<Employee>(genericRepositoryInterFace)
    {
    }
}
