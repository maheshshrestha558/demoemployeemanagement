﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLibrary.Dto
{
    public class EmployeeGroup2
    {
        [Required]
        public string JobName { get; set; } = string.Empty;
        [Required,Range(1,99999,ErrorMessage ="You have to selet branch")]
        public int BranchId { get; set; }
        [Required, Range(1, 99999, ErrorMessage = "You have to selet Town")]

        public int TownId { get; set; }
        [Required]
        public string? Other { get; set; }
    }
}
