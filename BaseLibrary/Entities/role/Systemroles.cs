﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLibrary.Entities.role
{
    public class Systemroles
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
