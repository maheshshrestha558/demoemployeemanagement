﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class Country:BaseEntities
    {
        //one to many relation with city
        [JsonIgnore]
        public List<City>? Cities { get; set; }  
    }
}
