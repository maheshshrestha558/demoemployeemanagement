﻿using BaseLibrary.Entities.@base;
using System.ComponentModel.DataAnnotations;

namespace BaseLibrary.Entities
{
    public class OverTime:OtherBaseEntities
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        public int NumberofDays => (EndDate - StartDate).Days;

        //many to one relation with vacation type

        public OvertimeType? OvertimeType { get; set; } 

        public int OvertimeTypeId { get; set; }


    }
}
