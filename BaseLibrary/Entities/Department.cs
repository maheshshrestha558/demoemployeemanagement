﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class Department:BaseEntities
    {
        //many to one relationship with GeneralDepartment

        public GeneralDepartment? GeneralDepartment { get; set; }

        public int GeneralDepartmentId { get; set; }


        //one to many relationship
        [JsonIgnore]
        public List<Branch>? Departments { get; set; }
        
    }
}
