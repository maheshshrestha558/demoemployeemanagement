﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities.@base
{
    public class BaseEntities
    {

        public int Id { get; set; }
        [Required]

        public string?  Name { get; set; }

        ////relationship : one to many
        //[JsonIgnore]
        //public List<Employee>? Employees { get; set; }

    }
}
