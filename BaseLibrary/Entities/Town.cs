﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class Town :BaseEntities
    {
        //one to many relationship with employee
        [JsonIgnore]
        public List<Employee>? Employees { get; set; }

        //many to one relationship with city
        public City? City { get; set; }
        public int CityId { get; set; }

        
    }
}
