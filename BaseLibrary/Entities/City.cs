﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class City:BaseEntities
    {
        //many to one relationship with country

        public Country? Country { get; set; } 
        public int CountryId { get; set; }


        //one to many relationship with town
        [JsonIgnore]
        public List<Town>? Towns { get; set; }
    
    
    }
}
