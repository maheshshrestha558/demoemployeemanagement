﻿using BaseLibrary.Entities.@base;
using System.ComponentModel.DataAnnotations;
namespace BaseLibrary.Entities.Leave
{
    public class Vacation : OtherBaseEntities
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public int NumberofDays { get; set; }

        public DateTime EndDate => StartDate.AddDays(NumberofDays);


        //many to one relationship with vacationtype

        public VacationType? VacationType { get; set; }

        [Required]
        public int VacationTypeId { get; set; }
    }
}
