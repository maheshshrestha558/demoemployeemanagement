﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities.Leave
{
    public class SanctionType : BaseEntities
    {
        //many to one relationship with vacation
        [JsonIgnore]
        public List<Sanction>? Sanctions { get; set; }
    }
}
