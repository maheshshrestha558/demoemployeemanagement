﻿using BaseLibrary.Entities.@base;
using System.ComponentModel.DataAnnotations;

namespace BaseLibrary.Entities.Leave
{
    public class Sanction : OtherBaseEntities
    {
        [Required]
        public DateTime Date { get; set; }
        [Required]

        public string Punishment { get; set; } = string.Empty;
        [Required]

        public DateTime PunishmentDate { get; set; }

        //many to one relationship with vacationtype

        public SanctionType? SanctionType { get; set; }
        public int SanctionTypeId { get; set; }


    }
}
