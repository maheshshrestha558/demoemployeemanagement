﻿using BaseLibrary.Entities.@base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class Branch : BaseEntities
    {
        //many to one relationship with Departments
        public Department? Department { get; set; }
        public int DepartmentId { get; set; }

        //one to many relationship
        [JsonIgnore]
        public List<Employee>? Employees { get; set; }
    }
}
