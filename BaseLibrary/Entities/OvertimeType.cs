﻿
using BaseLibrary.Entities.@base;
using BaseLibrary.Entities.Leave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BaseLibrary.Entities
{
    public class OvertimeType : BaseEntities
    {
        //many to one relationship with vacation
        [JsonIgnore]
        public List<OverTime>? OverTimes { get; set; }
    }
}
